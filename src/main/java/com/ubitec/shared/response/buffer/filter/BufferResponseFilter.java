package com.ubitec.shared.response.buffer.filter;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.client.ClientResponse;

@Provider
public class BufferResponseFilter implements ClientResponseFilter {
	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		ClientResponse response = (ClientResponse) responseContext;
		response.bufferEntity();
	}
}

