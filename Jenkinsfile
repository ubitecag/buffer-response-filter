pipeline {
	agent {
		label jenkinsAgents.commonFleet()
	}

	options {
		buildDiscarder(logRotator(daysToKeepStr: '14', numToKeepStr: '5'))
	}

	environment {
		repoUrl = "git@bitbucket.org:ubitecag/buffer-response-filter.git"
	}

	stages {
		stage("Checkout specific branch") {
			when {
				expression { params.buildBranch != null }
			}
			steps {
				checkout([$class: "GitSCM",
					branches: [[name: "${buildBranch}"]],
					extensions: [[$class: 'WipeWorkspace'], [$class: 'CloneOption', shallow: true]],
					userRemoteConfigs: [[
						url: "${repoUrl}",
						credentialsId: 'jenkins-ssh-private-key'
					]]
				])
			}
		}

		stage('Package') {
			agent {
				docker {
					/* !!! IMPORTANT:
					*     reuseNode forces Jenkins to run the pipeline on the same Jenkins Agent of previous
					*     stages.
					*/
					reuseNode true
					image ubitecDockerImage("io.ubitec/axonivy:8.0.8-jenkins")
					args "--shm-size 2g -v /var/jenkins_home/.m2:/var/jenkins_home/.m2 -e MAVEN_CONFIG=/var/jenkins_home/.m2"
				}
			}

			steps {
				configFileProvider([configFile(fileId: 'c4c35dc1-9743-4a5b-abff-6bef187ebdec', variable: 'GLOBAL_MAVEN_SETTINGS')]) {
					sh 'mkdir -p ~/.m2'
					sh 'cp ${GLOBAL_MAVEN_SETTINGS} ~/.m2/settings.xml'
				}
				mvn """clean package -U -X"""
			}
		}

		stage('Deploy Dev Repo') {
			when {
				environment name: 'deployDevRepo', value: 'true'
			}

			steps {
				script {
					ubitecRepoDeploy {
						toDev()
					}
				}
			}
		}
	}
}
